import base64
from hashlib import md5

from Crypto.Cipher import AES

BLOCK_SIZE = 16


def unpad(padded):
    pad = ord(padded[-1])
    return padded[:-pad]


def pad(data):
    pad = BLOCK_SIZE - len(data) % BLOCK_SIZE
    return data + pad * chr(pad)


def decrypt(data, secret):
    data = base64.urlsafe_b64decode(data)

    m = md5()
    m.update(secret.encode('utf-8'))
    key = m.hexdigest()

    m = md5()
    m.update((secret + key).encode('utf-8'))
    iv = m.hexdigest()

    aes = AES.new(key, AES.MODE_CBC, iv[:16])
    return unpad(aes.decrypt(data).decode('utf-8'))


def encrypt(data, secret):
    data = pad(data)

    m = md5()
    m.update(secret.encode('utf-8'))
    key = m.hexdigest()

    m = md5()
    m.update((secret + key).encode('utf-8'))
    iv = m.hexdigest()

    aes = AES.new(key, AES.MODE_CBC, iv[:16])
    encrypted = aes.encrypt(data)
    return base64.urlsafe_b64encode(encrypted)
