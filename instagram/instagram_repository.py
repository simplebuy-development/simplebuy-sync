from base_repository import BaseRepository
from instagram.api.instagram_api import InstagramAPI
from instagram.models.utils import decrypt

SECRET = "sb-secret"  # TODO: move to config via env variable


class InstagramRepository(BaseRepository):

    SESSIONS = {}

    def __init__(self, credentials, **kwargs):
        super().__init__(credentials)

        self.social_id = kwargs.get('id')

        if self.login not in self.SESSIONS.keys():
            self.api = InstagramAPI(self.login, decrypt(self.password, SECRET))
            self.SESSIONS[self.login] = self.api
        else:
            self.api = self.SESSIONS[self.login]

        # if not self.api.login():
        #     pass  # TODO: log error and maybe skip this integration

    @property
    def type(self):
        return 'in'

    @property
    def two_way_sync(self) -> bool:
        return False

    @property
    def login(self):
        return self.credentials['login']

    @property
    def password(self):
        return self.credentials['password']

    def save(self, item):
        if not self.api.is_logged_in:
            res = self.api.login()
            if res.status_code != 200:
                return res

        return self.api.upload_photo_by_url(item.photo, item.text)

    def sync_post(self, post):
        if not self.api.is_logged_in:
            res = self.api.login()
            if res.status_code != 200:
                return res

        hashtags = [("#" + ht["id"] if not ht["id"].startswith("#") else ht["id"]) for ht in post["_hashtags"]]
        product_links = []
        if len(post["product_list"]) > 0:
            photo = post["product_list"][0]["mainImage"]
            if "no_photo" in photo:
                photo = "http://simplebuy.org/images/no_photo.jpg"
            for product in post["product_list"]:
                product_links.append("%s: http://simplebuy.org/product/%s/details" % (product["name"], product["id"]))

        else:
            photo = post["author"]["avatar"]
            if "no_photo" in photo:
                photo = "http://simplebuy.org/images/no_photo.jpg"

        text = post["text"] + " ".join(hashtags) + " | " + " ".join(product_links)
        return self.api.upload_photo_by_url(photo, text)
