﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json
import hashlib
import hmac
import uuid
import time
import io
import logging
from urllib import parse
from instagram.api.image_utils import get_image_size
from requests_toolbelt import MultipartEncoder
from bs4 import BeautifulSoup
from requests.exceptions import ProxyError, ConnectionError


class Proxy:
    def __init__(self, ip, port, country, ping_time, proxy_type):
        self.ip = ip
        self.port = port
        self.country = country
        self.ping_time = ping_time
        self.proxy_type = proxy_type

    def __str__(self):
        return "Proxy server(IP: %s, port: %s, country: %s, ping time: %s, type: %s)" % \
               (self.ip, self.port, self.country, self.ping_time, self.proxy_type)

    def check(self):
        try:
            res = requests.get("https://www.instagram.com/", proxies={'https': 'https://%s:%s' % (self.ip, self.port)},
                               timeout=5)
            return True if res.status_code == 200 else False

        except (ProxyError, ConnectionError):
            return False


class InstagramAPI:
    API_URL = 'https://i.instagram.com/api/v1/'
    DEVICE_SETTINGS = {
        'manufacturer': 'Xiaomi',
        'model': 'HM 1SW',
        'android_version': 18,
        'android_release': '5.0'
    }
    USER_AGENT = 'Instagram 9.2.0 Android ({android_version}/{android_release}; 320dpi; 720x1280; {manufacturer}; ' \
                 '{model}; armani; qcom; en_US)'.format(**DEVICE_SETTINGS)
    IG_SIG_KEY = '012a54f51c49aa8c5c322416ab1410909add32c966bbaa0fe3dc58ac43fd7ede'
    EXPERIMENTS = 'ig_android_progressive_jpeg,ig_creation_growth_holdout,ig_android_report_and_hide,' \
                  'ig_android_new_browser,ig_android_enable_share_to_whatsapp,' \
                  'ig_android_direct_drawing_in_quick_cam_universe,ig_android_huawei_app_badging,' \
                  'ig_android_universe_video_production,ig_android_asus_app_badging,ig_android_direct_plus_button,' \
                  'ig_android_ads_heatmap_overlay_universe,ig_android_http_stack_experiment_2016,' \
                  'ig_android_infinite_scrolling,ig_fbns_blocked,ig_android_white_out_universe,' \
                  'ig_android_full_people_card_in_user_list,ig_android_post_auto_retry_v7_21,ig_fbns_push,' \
                  'ig_android_feed_pill,ig_android_profile_link_iab,ig_explore_v3_us_holdout,' \
                  'ig_android_histogram_reporter,ig_android_anrwatchdog,ig_android_search_client_matching,' \
                  'ig_android_high_res_upload_2,ig_android_new_browser_pre_kitkat,ig_android_2fac,' \
                  'ig_android_grid_video_icon,ig_android_white_camera_universe,ig_android_disable_chroma_subsampling,' \
                  'ig_android_share_spinner,ig_android_explore_people_feed_icon,ig_explore_v3_android_universe,' \
                  'ig_android_media_favorites,ig_android_nux_holdout,ig_android_search_null_state,' \
                  'ig_android_react_native_notification_setting,ig_android_ads_indicator_change_universe,' \
                  'ig_android_video_loading_behavior,ig_android_black_camera_tab,liger_instagram_android_univ,' \
                  'ig_explore_v3_internal,ig_android_direct_emoji_picker,ig_android_prefetch_explore_delay_time,' \
                  'ig_android_business_insights_qe,ig_android_direct_media_size,ig_android_enable_client_share,' \
                  'ig_android_promoted_posts,ig_android_app_badging_holdout,ig_android_ads_cta_universe,' \
                  'ig_android_mini_inbox_2,ig_android_feed_reshare_button_nux,ig_android_boomerang_feed_attribution,' \
                  'ig_android_fbinvite_qe,ig_fbns_shared,ig_android_direct_full_width_media,' \
                  'ig_android_hscroll_profile_chaining,ig_android_feed_unit_footer,ig_android_media_tighten_space,' \
                  'ig_android_private_follow_request,ig_android_inline_gallery_backoff_hours_universe,' \
                  'ig_android_direct_thread_ui_rewrite,ig_android_rendering_controls,' \
                  'ig_android_ads_full_width_cta_universe,ig_video_max_duration_qe_preuniverse,' \
                  'ig_android_prefetch_explore_expire_time,ig_timestamp_public_test,ig_android_profile,' \
                  'ig_android_dv2_consistent_http_realtime_response,ig_android_enable_share_to_messenger,' \
                  'ig_explore_v3,ig_ranking_following,ig_android_pending_request_search_bar,' \
                  'ig_android_feed_ufi_redesign,ig_android_video_pause_logging_fix,' \
                  'ig_android_default_folder_to_camera,ig_android_video_stitching_7_23,' \
                  'ig_android_profanity_filter,ig_android_business_profile_qe,ig_android_search,' \
                  'ig_android_boomerang_entry,ig_android_inline_gallery_universe,' \
                  'ig_android_ads_overlay_design_universe,ig_android_options_app_invite,' \
                  'ig_android_view_count_decouple_likes_universe,ig_android_periodic_analytics_upload_v2,' \
                  'ig_android_feed_unit_hscroll_auto_advance,ig_peek_profile_photo_universe,' \
                  'ig_android_ads_holdout_universe,ig_android_prefetch_explore,ig_android_direct_bubble_icon,' \
                  'ig_video_use_sve_universe,ig_android_inline_gallery_no_backoff_on_launch_universe,' \
                  'ig_android_image_cache_multi_queue,ig_android_camera_nux,ig_android_immersive_viewer,' \
                  'ig_android_dense_feed_unit_cards,ig_android_sqlite_dev,ig_android_exoplayer,' \
                  'ig_android_add_to_last_post,ig_android_direct_public_threads,' \
                  'ig_android_prefetch_venue_in_composer,ig_android_bigger_share_button,' \
                  'ig_android_dv2_realtime_private_share,ig_android_non_square_first,ig_android_video_interleaved_v2,' \
                  'ig_android_follow_search_bar,ig_android_last_edits,ig_android_video_download_logging,' \
                  'ig_android_ads_loop_count_universe,ig_android_swipeable_filters_blacklist,' \
                  'ig_android_boomerang_layout_white_out_universe,ig_android_ads_carousel_multi_row_universe,' \
                  'ig_android_mentions_invite_v2,ig_android_direct_mention_qe,' \
                  'ig_android_following_follower_social_context'

    SIG_KEY_VERSION = '4'

    @staticmethod
    def generate_device_id(seed):
        volatile_seed = "12345"
        m = hashlib.md5()
        m.update(seed.encode('utf-8') + volatile_seed.encode('utf-8'))
        return 'android-' + m.hexdigest()[:16]

    @staticmethod
    def generate_uuid(uuid_type):
        # according to https://github.com/LevPasha/Instagram-API-python/pull/16/files#r77118894
        # uuid = '%04x%04x-%04x-%04x-%04x-%04x%04x%04x' % (random.randint(0, 0xffff),
        #    random.randint(0, 0xffff), random.randint(0, 0xffff),
        #    random.randint(0, 0x0fff) | 0x4000,
        #    random.randint(0, 0x3fff) | 0x8000,
        #    random.randint(0, 0xffff), random.randint(0, 0xffff),
        #    random.randint(0, 0xffff))
        generated_uuid = str(uuid.uuid4())
        if uuid_type:
            return generated_uuid
        else:
            return generated_uuid.replace('-', '')

    def __init__(self, username, password):
        self.logger = logging.getLogger("InstagramAPI")
        m = hashlib.md5()
        m.update(username.encode('utf-8') + password.encode('utf-8'))
        self.device_id = self.generate_device_id(m.hexdigest())
        self.username = username
        self.password = password
        self.uuid = self.generate_uuid(True)
        self.is_logged_in = False
        self.last_response = None
        self.session = None
        self.last_json = None
        self.token = None
        self.rank_token = None
        self.username_id = None
        self.proxy = None

    def get_proxy(self, excluding_proxy=None):
        self.logger.info("[%s] get new proxy" % self.username)
        proxies = []
        html = requests.get("http://hideme.ru/proxy-list/?country=CANLRUSEGBUS&maxtime=1500&type=s#list")

        soup = BeautifulSoup(html.text, 'html.parser')
        for row in soup.find("table", class_="proxy__t").find_all("tr")[1:]:
            cells = row.find_all('td')
            proxies.append(Proxy(cells[0].get_text().strip(), cells[1].get_text().strip(), cells[2].get_text().strip(),
                                 cells[3].get_text().strip().split(' ')[0], cells[4].get_text().strip()))

        proxies.sort(key=lambda x: x.ping_time)
        for proxy in proxies:
            if proxy.check():
                if excluding_proxy and proxy.ip in excluding_proxy:
                    continue

                self.logger.info("[%s] new proxy is https://%s:%s" % (self.username, proxy.ip, proxy.port))
                return "https://%s:%s" % (proxy.ip, proxy.port)
        return None

    def login(self, force=False):
        if not self.is_logged_in or force:
            self.session = requests.Session()

            self.proxy = self.get_proxy() if not self.proxy else self.proxy
            if not self.proxy:
                self.logger.info("[%s] there is no available proxy" % self.username)
                return self.InstagramResponse(504, "Looks like your proxy is dead, please try again later")

            self.session.proxies = {"https": self.proxy}
            res = self.send_request('si/fetch_headers/?challenge_type=signup&guid=' + self.generate_uuid(False),
                                    None, True)
            if res.status_code == 200:

                data = {'phone_id': self.generate_uuid(True),
                        '_csrftoken': self.last_response.cookies['csrftoken'],
                        'username': self.username,
                        'guid': self.uuid,
                        'device_id': self.device_id,
                        'password': self.password,
                        'login_attempt_count': '0'}
                login_res = self.send_request('accounts/login/', self.generate_signature(json.dumps(data)), True)

                if login_res.status_code == 200:
                    self.is_logged_in = True
                    self.username_id = self.last_json["logged_in_user"]["pk"]
                    self.rank_token = "%s_%s" % (self.username_id, self.uuid)
                    self.token = self.last_response.cookies["csrftoken"]

                return login_res

            return res

    def sync_features(self):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'id': self.username_id,
            '_csrftoken': self.token,
            'experiments': self.EXPERIMENTS
        })
        return self.send_request('qe/sync/', self.generate_signature(data))

    def auto_complete_user_list(self):
        return self.send_request('friendships/autocomplete_user_list/')

    def timeline_feed(self):
        return self.send_request('feed/timeline/')

    def megaphone_log(self):
        return self.send_request('megaphone/log/')

    def expose(self):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'id': self.username_id,
            '_csrftoken': self.token,
            'experiment': 'ig_android_profile_contextual_feed'
        })
        return self.send_request('qe/expose/', self.generate_signature(data))

    def logout(self):
        self.send_request('accounts/logout/')

    def upload_photo(self, photo, caption=None, upload_id=None):
        if upload_id is None:
            upload_id = str(int(time.time() * 1000))
        data = {
            'upload_id': upload_id,
            '_uuid': self.uuid,
            '_csrftoken': self.token,
            'image_compression': '{"lib_name":"jt","lib_version":"1.3.0","quality":"87"}',
            'photo': ('pending_media_%s.jpg' % upload_id, open(photo, 'rb'), 'application/octet-stream',
                      {'Content-Transfer-Encoding': 'binary'})
        }
        m = MultipartEncoder(data, boundary=self.uuid)
        self.session.headers.update({'X-IG-Capabilities': '3Q4=',
                                     'X-IG-Connection-Type': 'WIFI',
                                     'Cookie2': '$Version=1',
                                     'Accept-Language': 'en-US',
                                     'Accept-Encoding': 'gzip, deflate',
                                     'Content-type': m.content_type,
                                     'Connection': 'close',
                                     'User-Agent': self.USER_AGENT})
        response = self.session.post(self.API_URL + "upload/photo/", data=m.to_string())
        if response.status_code == 200:
            if self.configure(upload_id, photo, caption):
                self.expose()
        return False

    def upload_photo_by_url(self, photo_url, caption=None):
        upload_id = str(int(time.time() * 1000))
        img_binary = requests.get(photo_url).content
        img_data = io.BytesIO(img_binary)
        data = {
            'upload_id': upload_id,
            '_uuid': self.uuid,
            '_csrftoken': self.token,
            'image_compression': '{"lib_name":"jt","lib_version":"1.3.0","quality":"87"}',
            'photo': ('pending_media_%s.jpg' % upload_id, img_data, 'application/octet-stream',
                      {'Content-Transfer-Encoding': 'binary'})
        }
        m = MultipartEncoder(data, boundary=self.uuid)
        self.session.headers.update({'X-IG-Capabilities': '3Q4=',
                                     'X-IG-Connection-Type': 'WIFI',
                                     'Cookie2': '$Version=1',
                                     'Accept-Language': 'en-US',
                                     'Accept-Encoding': 'gzip, deflate',
                                     'Content-type': m.content_type,
                                     'Connection': 'close',
                                     'User-Agent': self.USER_AGENT})
        upload_response = None
        try:
            for _ in range(3):
                upload_response = self.session.post(self.API_URL + "upload/photo/", data=m.to_string())
                if upload_response.status_code == 200:
                    break
                else:
                    self.logger.error("[%s] Error: upload/photo/: %s" % (self.username, upload_response.text))
                    if "login_required" in upload_response.text:
                        self.is_logged_in = False
                        self.logger.warning("[%s] Oops, looks like we should re-login: %s" %
                                            (self.username, upload_response.text))
                        self.login()
        except (ProxyError, ConnectionError):
            return self.InstagramResponse(504, "Can't upload photo due to proxy error. Try again later")

        if not upload_response or upload_response.status_code != 200:
            return upload_response

        return self.configure(upload_id, img_data, caption)

    def configure(self, upload_id, photo, caption=''):
        (w, h) = get_image_size(photo)
        data = json.dumps({
            '_csrftoken': self.token,
            'media_folder': 'Instagram',
            'source_type': 4,
            '_uid': self.username_id,
            '_uuid': self.uuid,
            'caption': caption,
            'upload_id': upload_id,
            'device': self.DEVICE_SETTINGS,
            'edits': {
                'crop_original_size': [w * 1.0, h * 1.0],
                'crop_center': [0.0, 0.0],
                'crop_zoom': 1.0
            },
            'extra': {
                'source_width': w,
                'source_height': h,
            }
        })
        return self.send_request('media/configure/?', self.generate_signature(data))

    def edit_media(self, media_id, caption_text=''):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'caption_text': caption_text
        })
        return self.send_request('media/' + str(media_id) + '/edit_media/', self.generate_signature(data))

    def remove_self_tag(self, media_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token
        })
        return self.send_request('media/' + str(media_id) + '/remove/', self.generate_signature(data))

    def media_info(self, media_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'media_id': media_id
        })
        return self.send_request('media/' + str(media_id) + '/info/', self.generate_signature(data))

    def delete_media(self, media_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'media_id': media_id
        })
        return self.send_request('media/' + str(media_id) + '/delete/', self.generate_signature(data))

    def change_password(self, new_password):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'old_password': self.password,
            'new_password1': new_password,
            'new_password2': new_password
        })
        return self.send_request('accounts/change_password/', self.generate_signature(data))

    def explore(self):
        return self.send_request('discover/explore/')

    def comment(self, media_id, comment_text):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'comment_text': comment_text
        })
        return self.send_request('media/' + str(media_id) + '/comment/', self.generate_signature(data))

    def delete_comment(self, media_id, comment_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token
        })
        return self.send_request('media/' + str(media_id) + '/comment/' + str(comment_id) + '/delete/',
                                 self.generate_signature(data))

    def remove_profile_picture(self):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token
        })
        return self.send_request('accounts/remove_profile_picture/', self.generate_signature(data))

    def set_private_account(self):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token
        })
        return self.send_request('accounts/set_private/', self.generate_signature(data))

    def set_public_account(self):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token
        })
        return self.send_request('accounts/set_public/', self.generate_signature(data))

    def get_profile_data(self):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token
        })
        return self.send_request('accounts/current_user/?edit=true', self.generate_signature(data))

    def edit_profile(self, url, phone, first_name, biography, email, gender):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'external_url': url,
            'phone_number': phone,
            'username': self.username,
            'full_name': first_name,
            'biography': biography,
            'email': email,
            'gender': gender,
        })
        return self.send_request('accounts/edit_profile/', self.generate_signature(data))

    def get_username_info(self, username_id):
        return self.send_request('users/' + str(username_id) + '/info/')

    def get_self_username_info(self):
        return self.get_username_info(self.username_id)

    def get_recent_activity(self):
        return self.send_request('news/inbox/?')

    def get_following_recent_activity(self):
        return self.send_request('news/?')

    def get_v2_inbox(self):
        return self.send_request('direct_v2/inbox/?')

    def get_user_tags(self, username_id):
        return self.send_request('usertags/' + str(username_id) + '/feed/?rank_token=' + str(self.rank_token) +
                                 '&ranked_content=true&')

    def get_self_user_tags(self):
        return self.get_user_tags(self.username_id)

    def tag_feed(self, tag):
        return self.send_request('feed/tag/' + str(tag) + '/?rank_token=' + str(self.rank_token) +
                                 '&ranked_content=true&')

    def get_media_likers(self, media_id):
        return self.send_request('media/' + str(media_id) + '/likers/?')

    def get_geo_media(self, username_id):
        return self.send_request('maps/user/' + str(username_id) + '/')

    def get_self_geo_media(self):
        return self.get_geo_media(self.username_id)

    def fb_user_search(self, query):
        return self.send_request('fbsearch/topsearch/?context=blended&query=' + str(query) + '&rank_token=' +
                                 str(self.rank_token))

    def search_users(self, query):
        return self.send_request('users/search/?ig_sig_key_version=' + str(self.SIG_KEY_VERSION) +
                                 '&is_typeahead=true&query=' + str(query) + '&rank_token=' + str(self.rank_token))

    def search_username(self, username_name):
        return self.send_request('users/' + str(username_name) + '/usernameinfo/')

    def sync_from_address_book(self, contacts):
        return self.send_request('address_book/link/?include=extra_display_name,thumbnails', json.dumps(contacts))

    def search_tags(self, query):
        return self.send_request('tags/search/?is_typeahead=true&q=' + str(query) + '&rank_token=' +
                                 str(self.rank_token))

    def get_timeline(self):
        return self.send_request('feed/timeline/?rank_token=' + str(self.rank_token) + '&ranked_content=true&')

    def get_user_feed(self, username_id, max_id='', min_timestamp=None):
        return self.send_request('feed/user/' + str(username_id) + '/?max_id=' + str(max_id) + '&min_timestamp=' +
                                 str(min_timestamp) + '&rank_token=' + str(self.rank_token) + '&ranked_content=true')

    def get_self_user_feed(self, max_id='', min_timestamp=None):
        return self.get_user_feed(self.username_id, max_id, min_timestamp)

    def get_hashtag_feed(self, hashtag_string, max_id=''):
        return self.send_request('feed/tag/' + hashtag_string + '/?max_id=' +
                                 str(max_id) + '&rank_token=' + self.rank_token + '&ranked_content=true&')

    def search_location(self, query):
        return self.send_request('fbsearch/places/?rank_token=' + str(self.rank_token) + '&query=' + str(query))

    def get_location_feed(self, location_id, max_id=''):
        return self.send_request('feed/location/' + str(location_id) + '/?max_id=' + max_id + '&rank_token=' +
                                 self.rank_token + '&ranked_content=true&')

    def get_popular_feed(self):
        return self.send_request('feed/popular/?people_teaser_supported=1&rank_token=' + str(self.rank_token) +
                                 '&ranked_content=true&')

    def get_user_followings(self, username_id, max_id=''):
        return self.send_request('friendships/' + str(username_id) + '/following/?max_id=' + str(max_id)
                                 + '&ig_sig_key_version=' + self.SIG_KEY_VERSION + '&rank_token=' + self.rank_token)

    def get_self_users_following(self):
        return self.get_user_followings(self.username_id)

    def get_user_followers(self, username_id, max_id=''):
        return self.send_request('friendships/' + str(username_id) + '/followers/?max_id=' + str(max_id)
                                 + '&ig_sig_key_version=' + self.SIG_KEY_VERSION + '&rank_token=' + self.rank_token)

    def get_self_user_followers(self):
        return self.get_user_followers(self.username_id)

    def like(self, media_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'media_id': media_id
        })
        return self.send_request('media/' + str(media_id) + '/like/', self.generate_signature(data))

    def unlike(self, media_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            '_csrftoken': self.token,
            'media_id': media_id
        })
        return self.send_request('media/' + str(media_id) + '/unlike/', self.generate_signature(data))

    def get_media_comments(self, media_id):
        return self.send_request('media/' + media_id + '/comments/?')

    def set_name_and_phone(self, name='', phone=''):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'first_name': name,
            'phone_number': phone,
            '_csrftoken': self.token
        })
        return self.send_request('accounts/set_phone_and_name/', self.generate_signature(data))

    def get_direct_share(self):
        return self.send_request('direct_share/inbox/?')

    def follow(self, user_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'user_id': user_id,
            '_csrftoken': self.token
        })
        return self.send_request('friendships/create/' + str(user_id) + '/', self.generate_signature(data))

    def unfollow(self, user_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'user_id': user_id,
            '_csrftoken': self.token
        })
        return self.send_request('friendships/destroy/' + str(user_id) + '/', self.generate_signature(data))

    def block(self, user_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'user_id': user_id,
            '_csrftoken': self.token
        })
        return self.send_request('friendships/block/' + str(user_id) + '/', self.generate_signature(data))

    def unblock(self, user_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'user_id': user_id,
            '_csrftoken': self.token
        })
        return self.send_request('friendships/unblock/' + str(user_id) + '/', self.generate_signature(data))

    def user_friendship(self, user_id):
        data = json.dumps({
            '_uuid': self.uuid,
            '_uid': self.username_id,
            'user_id': user_id,
            '_csrftoken': self.token
        })
        return self.send_request('friendships/show/' + str(user_id) + '/', self.generate_signature(data))

    def get_liked_media(self, max_id=''):
        return self.send_request('feed/liked/?max_id=' + str(max_id))

    def generate_signature(self, data):
        return 'ig_sig_key_version=' + self.SIG_KEY_VERSION + '&signed_body=' + hmac.new(
            self.IG_SIG_KEY.encode('utf-8'), data.encode('utf-8'),
            hashlib.sha256).hexdigest() + '.' + parse.quote(data)

    class InstagramResponse:
        def __init__(self, status_code, text):
            self.status_code = status_code
            self.text = text

    def send_request(self, endpoint, post=None, login=False):
        if not self.is_logged_in and not login:
            raise Exception("Not logged in!\n")

        self.session.headers.update({'Connection': 'close',
                                     'Accept': '*/*',
                                     'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                                     'Cookie2': '$Version=1',
                                     'Accept-Language': 'en-US',
                                     'User-Agent': self.USER_AGENT})
        response = None
        for _ in range(3):
            try:
                if post:  # POST
                    response = self.session.post(self.API_URL + endpoint, data=post)  # , verify=False
                else:  # GET
                    response = self.session.get(self.API_URL + endpoint)  # , verify=False

                if response.status_code == 200:
                    break

            except (ProxyError, ConnectionError) as err:
                self.logger.error("[%s] Proxy error: %s. Lets use another one" % (self.username, err))
                self.session.proxies = self.get_proxy(self.proxy)

        if response is None:
            self.logger.error("[%s] %s" % (self.username, "Looks like all proxies are dead, please try again later"))
            return self.InstagramResponse(504, "Looks like your proxy is dead, please try again later")

        if response.status_code == 200:
            self.last_response = response
            self.last_json = json.loads(response.text)
        else:
            self.logger.error("[%s] Request return %s error: %s" % (self.username, response.status_code, response.text))

        return response

    def get_total_followers(self, username_id):
        followers = []
        next_max_id = ''
        while 1:
            self.get_user_followers(username_id, next_max_id)
            temp = self.last_json

            for item in temp["users"]:
                followers.append(item)

            if not temp["big_list"]:
                return followers
            next_max_id = temp["next_max_id"]

    def get_total_followings(self, username_id):
        followers = []
        next_max_id = ''
        while 1:
            self.get_user_followings(username_id, next_max_id)
            temp = self.last_json

            for item in temp["users"]:
                followers.append(item)

            if not temp["big_list"]:
                return followers
            next_max_id = temp["next_max_id"]

    def get_total_user_feed(self, username_id, min_timestamp=None):
        user_feed = []
        next_max_id = ''
        while 1:
            self.get_user_feed(username_id, next_max_id, min_timestamp)
            temp = self.last_json
            for item in temp["items"]:
                user_feed.append(item)
            if not temp["more_available"]:
                return user_feed
            next_max_id = temp["next_max_id"]

    def get_total_self_user_feed(self, min_timestamp=None):
        return self.get_total_user_feed(self.username_id, min_timestamp)

    def get_total_self_followers(self):
        return self.get_total_followers(self.username_id)

    def get_total_self_followings(self):
        return self.get_total_followings(self.username_id)

    def get_total_liked_media(self, scan_rate=1):
        next_id = ''
        liked_items = []
        for x in range(0, scan_rate):
            self.get_liked_media(next_id)
            temp = self.last_json
            next_id = temp["next_max_id"]
            for item in temp["items"]:
                liked_items.append(item)
        return liked_items
