import time
import logging
import json
from datetime import datetime
from threading import Thread
from typing import List

from requests import HTTPError

from base_repository import TwoWaySyncRepository
from product_converter import ProductConverter
from sb.converters.to_in_product_converter import ToInProductConverter as Sb2In
from sb.converters.to_vk_product_converter import ToVkProductConverter as Sb2Vk
from sb.models.product import Product
from vk.converters.to_sb_product_converter import ToSbProductConverter as Vk2Sb

PRODUCT_CONVERTERS = {
    'vk': {
        'sb': Vk2Sb
    },
    'sb': {
        'vk': Sb2Vk,
        'in': Sb2In
    }
}


def get_converter(from_type, to_type) -> ProductConverter:
    to_converters = PRODUCT_CONVERTERS.get(from_type)
    if not to_converters:
        return None
    return to_converters.get(to_type)()


def products_equal(a, b):
    return (
        a.title == b.title
        and a.price == b.price
        and a.description == b.description # TODO: need to impl hashtags parsing in all slave repos at first
        # and hashtags_equal(a.hashtags, b.hashtags) # TODO: need to impl hashtags parsing in all slave repos at first
    )


def hashtags_equal(a, b=None):
    if b is None:
        b = []
    for ht in a:
        found = next(filter(lambda x: x.id == ht.id, b), None)
        if not found:
            return False
    return True


class SyncWorker(Thread):
    def __init__(self, workers, store_id, main_repo, slave_repos):
        super().__init__(name=store_id)
        self.export_date = None
        self.workers = workers
        self.store_id = store_id
        self.main_repo = main_repo
        self.slave_repos = slave_repos
        self.products = []  # type: List[Product]
        self.domain_name = None

    def run(self):
        self.workers[self.name] = self
        try:
            self.sync()
        finally:
            del self.workers[self.name]

    def sync(self):
        # get domain name
        data = self.main_repo.api.domain(self.store_id).get()
        if len(data) > 0:
            self.domain_name = data[0]['name']

        # Import
        self.cache_products(self.main_repo.get_products_to_sync())
        two_way_repos = [repo for repo in self.slave_repos if repo.two_way_sync]

        # create default results for all repos
        sync_result = {}
        for repo in self.slave_repos:
            sync_result[repo] = {"syncData": {"date": "", "status": "SUCCESS", "messages": []}}

        for repo in two_way_repos:
            errors = self.sync_slave_to_master(repo)
            if errors:
                sync_result[repo] = {"syncData": {"date": "", "status": "FAILURE", "messages": errors}}

        # Export
        self.export_date = int(datetime.utcnow().timestamp())
        self.cache_products(self.main_repo.get_products_to_sync())

        for repo in self.slave_repos:
            errors = self.sync_master_to_slave(repo)
            if errors:
                sync_result[repo]['syncData']['status'] = "FAILURE"
                sync_result[repo]['syncData']['messages'] += errors
            sync_result[repo]['syncData']['date'] = time.time()
            self.main_repo.api.socials(repo.social_id).update_sync_data(sync_result[repo])

    def sync_slave_to_master(self, repo: TwoWaySyncRepository):
        converter = get_converter(repo.type, self.main_repo.type)
        if not converter:
            logging.debug("Cannot find converter from %s to %s" % (repo.type, self.main_repo.type))
            return
        errors = []
        for item in repo.get_products_to_sync():
            res = self.import_item(item, converter, repo)
            if res:
                errors.append("Failed to import product [%s] due to %s" % (item, res))

        return errors

    def import_item(self, item, converter, repo):
        try:

            imported_product = converter.convert(item, self.main_repo)
            cached_product = self.get_from_cache(item.id, repo.type)
            if cached_product:
                imported_product.id = cached_product.id
                imported_product.revision = cached_product.revision
            if self.need_import(cached_product, imported_product, repo.type):
                response = self.main_repo.save(imported_product)
                saved_product = Product(**response)
                self.main_repo.mark_as_synced(saved_product, repo.type, item.id)

            if repo.type == 'vk':
                imported_product = converter.convert(item, self.main_repo)
                conv = get_converter(self.main_repo.type, repo.type)
                vk_product = conv.convert(cached_product, repo,
                                          {'store_id': self.store_id,
                                           'domain_name': self.domain_name})
                if item.description != vk_product.description:
                    vk_product.id = item.id
                    repo.save_market_item(vk_product)

            return None
        except HTTPError as httpError:
            logging.warning('Save product error during import: %s', httpError)
            return 'Save product error during import: %s', httpError
        except Exception as err:
            logging.warning('Other import item error: %s', err)
            return 'Unexpected error while import item: %s', err

    @staticmethod
    def need_import(cached: Product, imported: Product, import_type):
        """
        Calculate if product is new for Master OR it's not expired for this integration and changed in Slave
        """
        return not cached or (
            not cached.is_expired(import_type) and not products_equal(imported, cached))

    def sync_master_to_slave(self, repo):
        converter = get_converter(self.main_repo.type, repo.type)
        if not converter:
            logging.debug("Cannot find converter from %s to %s" % (self.main_repo.type, repo.type))
            return
        errors = []
        for product in self.products:
            res = self.export_product(product, converter, repo)
            if res:
                errors.append("Failed to export product [%s] due to %s" % (product.id, res))

        return errors

    def export_product(self, product, converter, repo):
        try:
            if product.is_expired(repo.type):
                converted_item = converter.convert(product, repo)

                res = repo.save(converted_item)
                if repo.type == 'in':
                    if res.status_code == 200:
                        res = json.loads(res.text)['media']['code']
                    else:
                        return res.text

                self.main_repo.mark_as_synced(product, repo.type, res)
                return None
        except HTTPError as httpError:
            logging.warning('HTTP error during export product: %s', httpError)
            return 'HTTP error during export product: %s', httpError
        except Exception as err:
            logging.warning('Other export product error: %s', err)
            return 'Unexpected error while export: %s', err

    def cache_products(self, products):
        self.products = products

    def get_from_cache(self, item_id, repo_type):
        for product in self.products:
            sd = product.get_sync_data(repo_type)
            if item_id == sd['id']:
                return product


class PostSyncWorker(Thread):
    def __init__(self, post, repository, api):
        super().__init__(name=post['id'])
        self.post = post
        self.repository = repository
        self.api = api

    def run(self):
        sync_data = {'date': '', 'status': '', 'message': '', 'type': 'in'}
        status = 500
        message = "Failed to sync %s - Unknown error" % self.post['id']

        try:
            res = self.repository.sync_post(self.post)

            if res.status_code == 200:
                message = json.loads(res.text)['media']['code']
            else:
                message = res.text

            status = res.status_code

        except HTTPError as httpError:
            logging.warning('HTTP error during export product: %s', httpError)
            status = 500
            message = 'HTTP error during export product: %s' % httpError
        except Exception as err:
            logging.warning('Other export product error: %s', err)
            status = 500
            message = 'Other export product error: %s' % err

        finally:
            sync_data['date'] = time.time()
            sync_data['status'] = status
            sync_data['message'] = message
            self.api.news(self.post['id']).update_sync_data(sync_data)


class MessagesSyncWorker(Thread):
    def __init__(self, store_id, main_repo, slave_repos):
        super().__init__(name=store_id)
        self.store_id = store_id
        self.main_repo = main_repo
        self.slave_repos = slave_repos
        self.sb_chats = []

    def is_chat_exist(self, chat):
        for ch in self.sb_chats:
            if ch.object_id == chat.object_id:
                return ch
        return False

    def run(self):
        self.sb_chats = self.main_repo.get_all_chats()
        for repo in self.slave_repos:
            r_chats = repo.get_all_chats()
            for rch in r_chats:
                sb_chat = self.is_chat_exist(rch)
                if not sb_chat:
                    sb_chat = self.main_repo.create_chat(rch.convert_to_sb(self.main_repo))

                messages = repo.get_post_messages(rch, offset=sb_chat.message_count)
                for msg in messages:
                    self.main_repo.add_message(sb_chat, msg.convert_to_sb(sb_chat))
