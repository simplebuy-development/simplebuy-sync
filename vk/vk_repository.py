import io
import math
import logging
import requests
from vk_api import VkApi

from base_repository import TwoWaySyncRepository
from vk.models.market.market_item import MarketItem, FullMarketItem
from vk.models.post.post import Post
from vk.models.post.message import Message
app_id = 5675657
api_version = '5.59'


class VkRepository(TwoWaySyncRepository):
    def __init__(self, credentials, **kwargs):
        super().__init__(credentials)
        self.social_id = kwargs.get('id')
        vk_session = VkApi(token=self.token, api_version=api_version, app_id=app_id)
        self.vk = vk_session.get_api()
        self.owner_id = None
        self.group_id = None

        user = kwargs.get('user', None)
        if user:
            self.owner_id = user['id']

        group = kwargs.get('group', None)
        if group:
            self.group_id = group['id']

        if self.group_id:
            self.owner_id = -self.group_id
        else:
            raise KeyError('owner_id or group_id must be defined')

    @property
    def type(self):
        return 'vk'

    @property
    def two_way_sync(self) -> bool:
        return True

    @property
    def token(self):
        return self.credentials['accessToken']

    def save(self, item: FullMarketItem):
        return self.save_market_item(item)

    def get_products_to_sync(self):
        return self.get_market_items()

    def mark_as_synced(self, item, repo_type, external_id):
        pass

    def upload_market_photo(self, photo_url):
        img_binary = requests.get(photo_url).content
        img_data = io.BytesIO(img_binary)
        url_response = self.vk.photos.getMarketUploadServer(main_photo=1, group_id=math.fabs(self.owner_id))
        logging.info("DEBUG: getMarketUploadServer response: %s" % url_response)
        upload_response = requests.post(url_response['upload_url'], files={'file': ('file.jpg', img_data)}).json()
        upload_response['group_id'] = int(math.fabs(self.owner_id))
        logging.info("DEBUG: Upload response: %s" % upload_response)
        save_response = self.vk.photos.saveMarketPhoto(**upload_response)
        logging.info("DEBUG: saveMarketPhoto response: %s" % save_response)
        return save_response[0]

    def save_market_item(self, mi: FullMarketItem):
        if mi.id:
            main_photo_data = self.upload_market_photo(mi.thumb_photo)  # TODO: probably get first from mi.photos
            self.vk.market.edit(
                owner_id=mi.owner_id,
                item_id=mi.id,
                name=mi.title,
                description=mi.description,
                category_id=mi.category.id,
                price=mi.price.amount,
                main_photo_id=main_photo_data['id'])

            return mi.id
        else:
            main_photo_data = self.upload_market_photo(mi.thumb_photo)  # TODO: probably get first from mi.photos
            result = self.vk.market.add(
                owner_id=mi.owner_id,
                name=mi.title,
                description=mi.description,
                category_id=mi.category.id,
                price=mi.price.amount,
                main_photo_id=main_photo_data['id'])

            return result['market_item_id']

    def get_market_items(self):
        response = self.vk.market.get(owner_id=self.owner_id, extended=1)
        return [MarketItem(**item) for item in response['items']]

    def sync_post(self, post):
        pass

    def get_all_chats(self):
        response = self.vk.wall.get(owner_id=self.owner_id, filter="all", count=100, offset=0)
        for item in response['items']:
            item['group_id'] = abs(self.group_id)

        return [Post(**item) for item in response['items']]

    def get_post_messages(self, post, offset):

        response = self.vk.wall.getComments(owner_id=self.owner_id, post_id=post.id, offset=offset, count=100,
                                            extended=1)
        
        def get_profile(user_id, profile_list):
            for prof in profile_list:
                if prof['id'] == user_id:
                    return prof

            return None            

        profiles = response['profiles']
        items = response['items']
        messages = []
        for item in items:
            profile = get_profile(item['from_id'], profiles)
            data = {
                'id': item['id'],
                'text': item['text'],
                'date': item['date'],
                'from_id': item['from_id'],
                'photo_100': profile['photo_100'],
                'name': profile['first_name'] + " " + profile['last_name'],
                'screen_name': profile['screen_name'],
            }

            messages.append(Message(**data))
        return messages
