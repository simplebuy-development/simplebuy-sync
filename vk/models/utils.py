def array_of_type(values, clazz):
    return None if values is None else [clazz(**v) for v in values]
