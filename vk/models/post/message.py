class Message:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.photo_100 = kwargs['photo_100']
        self.text = kwargs['text']
        self.date = kwargs['date']
        self.from_id = kwargs['from_id']
        self.name = kwargs['name']
        self.screen_name = kwargs['screen_name']

    def convert_to_sb(self, sb_chat):
        data = {'chatId': sb_chat.id,
                'storeId': sb_chat.store_id,
                'timestamp': int(self.date) * 1000,
                'authorName': self.name,
                'authorId': self.from_id,
                'authorPhoto': self.photo_100,
                'authorType': 'VK',
                'text': self.text,
                'unread': True
                }
        return data
