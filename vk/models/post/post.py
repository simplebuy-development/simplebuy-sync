
class Post:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.from_id = kwargs['from_id']
        self.text = kwargs.get('text', '')
        self.type = kwargs['post_type']
        self.comments_count = kwargs['comments']['count']
        self.likes_count = kwargs['likes']['count']
        self.repost_count = kwargs['reposts']['count']
        self.object_id = str(kwargs['group_id']) + "_" + str(self.id)
        self.attachments = kwargs.get('attachments', [])
    
    def convert_to_sb(self, sb_repo):

        chatImage = None

        for attachment in self.attachments:
            if attachment['type'] == 'photo':
                chatImage = attachment['photo']['photo_75']
        if not chatImage:
            if not sb_repo.store_avatar:
                chatImage = sb_repo.api.store(sb_repo.store_id).get()['avatar']
                sb_repo.store_avatar = chatImage
            else:
                chatImage = sb_repo.store_avatar

        chat_data = {
            "chatName": "Пост в ВК - " + self.text[:32] + ("..." if len(self.text) > 32 else ""),
            "chatType": "VK_POST",
            "chatImage": chatImage,
            "objectId": self.object_id,
            "storeId": sb_repo.store_id
        }

        return chat_data
