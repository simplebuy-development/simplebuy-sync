from vk.models.market.currency import Currency


class Price:
    def __init__(self, **kwargs):
        self.amount = kwargs.get('amount')
        self.currency = Currency(**kwargs['currency']) if kwargs.get('currency') else None
        self.text = kwargs.get('text')
