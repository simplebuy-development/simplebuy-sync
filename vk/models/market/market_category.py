from vk.models.market.section import Section


class MarketCategory:
    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.name = kwargs['name']
        self.section = Section(**kwargs['section'])
