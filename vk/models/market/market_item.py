from enum import IntEnum

from vk.models.base.likes import BaseLikes
from vk.models.market.market_category import MarketCategory
from vk.models.photos.photo import Photo

from vk.models.market.price import Price


class MarketItemAvailability(IntEnum):
    available = 0
    removed = 1
    unavailable = 2


class MarketItem:
    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.owner_id = kwargs['owner_id']
        self.title = kwargs['title']
        self.description = kwargs['description']
        self.price = Price(**kwargs['price'])
        self.category = MarketCategory(**kwargs['category'])
        self.date = kwargs['date']
        self.thumb_photo = kwargs['thumb_photo']
        self.availability = MarketItemAvailability(kwargs['availability'])


class FullMarketItem(MarketItem):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.photos = [Photo(**p) for p in kwargs.get('photos', [])]
        self.can_comment = bool(kwargs.get('can_comment'))
        self.can_repost = bool(kwargs.get('can_repost'))
        self.likes = BaseLikes(**kwargs.get('likes'))
