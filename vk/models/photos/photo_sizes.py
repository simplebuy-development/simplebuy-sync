class PhotoSizes:
    def __init__(self, **kwargs):
        self.src = kwargs['src']
        self.width = kwargs['width']
        self.height = kwargs['height']
        self.type = kwargs['type']  # TODO: may be make as enum of ['s','m','x','o','p','q','r','y','z','w']
