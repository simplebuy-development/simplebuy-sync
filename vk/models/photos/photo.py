from vk.models.photos.photo_sizes import PhotoSizes


class Photo:
    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.album_id = kwargs['album_id']
        self.owner_id = kwargs['owner_id']
        self.user_id = kwargs.get('user_id')
        self.sizes = [PhotoSizes(**ps) for ps in kwargs.get('sizes', [])]
        self.photo_75 = kwargs.get('photo_75')
        self.photo_130 = kwargs.get('photo_130')
        self.photo_604 = kwargs.get('photo_604')
        self.photo_807 = kwargs.get('photo_807')
        self.photo_1280 = kwargs.get('photo_1280')
        self.post_id = kwargs.get('post_id')
        self.width = kwargs.get('width')
        self.height = kwargs.get('height')
        self.text = kwargs.get('text')
        self.date = kwargs['date']  # type: int
        self.lat = kwargs.get('lat')
        self.long = kwargs.get('long')
        self.access_key = kwargs.get('access_key')
