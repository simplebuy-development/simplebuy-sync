class BaseLikes:
    def __init__(self, **kwargs):
        self.user_likes = bool(kwargs.get('user_likes'))
        self.count = kwargs.get('count')
