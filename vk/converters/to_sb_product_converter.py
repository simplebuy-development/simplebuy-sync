from product_converter import ProductConverter
from sb.models.product import Product
from sb.sb_repository import SbRepository
from vk.models.market.market_item import MarketItem


class ToSbProductConverter(ProductConverter):
    def convert(self, product: MarketItem, repo: SbRepository = None):
        description = product.description.split('Купить: http')[0].strip()

        tags = []
        for word in product.description.split():
            if word.startswith("#") and word not in tags:
                tags.append(word)

        ht = [{'id': tag} for tag in tags]
        product = Product(
            storeId=repo.store_id,
            storeName=repo.store_name or 'Store %s' % str(repo.store_id),
            name=product.title,
            description=description,
            _hashtags=ht,
            price=float(product.price.amount) / 100,
            mainImage=product.thumb_photo,
            images=[product.thumb_photo]  # TODO: get photo with maximum size from FullMarketItem
        )
        return product
