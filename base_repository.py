class BaseRepository:
    def __init__(self, credentials):
        self.credentials = credentials

    @property
    def type(self):
        raise NotImplementedError("Please override this property")

    def save(self, item):
        raise NotImplementedError("Please implement this method which should save item into repository")


class TwoWaySyncRepository(BaseRepository):
    @property
    def type(self):
        raise NotImplementedError("Please override this property")

    def save(self, item):
        raise NotImplementedError("Please implement this method which should save item into repository")

    @property
    def two_way_sync(self) -> bool:
        raise NotImplementedError("Please override this property")

    def get_products_to_sync(self):
        raise NotImplementedError("Please implement this method which should return list of ready-to-sync products")

    def mark_as_synced(self, item, repo_type, external_id):
        raise NotImplementedError("Please implement this method which should mark product as synced")
