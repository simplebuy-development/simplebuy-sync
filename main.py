import logging

from flask import Flask, jsonify
from flask import request
from werkzeug.exceptions import BadRequest
from requests.exceptions import HTTPError
from instagram.instagram_repository import InstagramRepository
from sb.sb_repository import SbRepository
from sync_worker import SyncWorker, PostSyncWorker, MessagesSyncWorker
from vk.vk_repository import VkRepository
from sb import sb_api

logging.basicConfig(level=logging.DEBUG)

REPOSITORIES = {
    'sb': SbRepository,
    'vk': VkRepository,
    'in': InstagramRepository
}

app = Flask(__name__)


def read_integrations(body):
    if type(body) == list:
        return [init_repo(**i) for i in body]


def init_repo(type, credentials, **kwargs):
    repo = REPOSITORIES.get(type)
    return repo(credentials, **kwargs)


def send_error(text, code=500):
    response = jsonify(error=text)
    response.status_code = code
    return response


# Used in SyncWorker to adding/removing itself in this dictionary
workers = {}  # TODO: should be moved to external Key-Value storage to support scaling


@app.route('/v1/sync/store/<store_id>', methods=['POST'])
def sync_store(store_id):
    try:
        auth_header = request.headers.get('Authorization', '').split(' ')
        if len(auth_header) < 2 or len(auth_header[1]) == 0:
            return send_error('Sync request has invalid authorization token', 401)
        sb_token = auth_header[1]

        try:
            integrations = request.json
        except BadRequest:
            return send_error('Request body must contain a valid JSON', 400)

        if workers.get(store_id):
            return send_error('Previous synchronization is not completed yet', 409)

        slave_repos = read_integrations(integrations)
        try:
            main_repo = SbRepository({'token': sb_token}, store_id)
        except HTTPError as err:
            logging.error("Error: %s." % err)
            return send_error('Error while initializing SB repository - %s' % err, 400)

        if len(slave_repos) == 0:
            return send_error('None of the provided integration is not valid', 422)

        worker = SyncWorker(workers, store_id, main_repo, slave_repos)
        worker.start()
        return jsonify(workerId=worker.name)
    except:
        sync_fail('Error during synchronization!')
        raise


@app.route('/v1/sync/messages/<store_id>', methods=['POST'])
def sync_messages(store_id):
    try:
        auth_header = request.headers.get('Authorization', '').split(' ')
        if len(auth_header) < 2 or len(auth_header[1]) == 0:
            return send_error('Sync request has invalid authorization token', 401)
        sb_token = auth_header[1]

        try:
            integrations = request.json
        except BadRequest:
            return send_error('Request body must contain a valid JSON', 400)

        if workers.get(store_id):
            return send_error('Previous synchronization is not completed yet', 409)

        slave_repos = read_integrations(integrations)
        try:
            main_repo = SbRepository({'token': sb_token}, store_id)
        except HTTPError as err:
            logging.error("Error: %s." % err)
            return send_error('Error while initializing SB repository - %s' % err, 400)

        if len(slave_repos) == 0:
            return send_error('None of the provided integration is not valid', 422)

        worker = MessagesSyncWorker(store_id, main_repo, slave_repos)
        worker.start()
        return jsonify(workerId=worker.name)
    except:
        sync_fail('Error during synchronization!')
        raise


@app.route('/v1/sync/post/<post_id>', methods=['POST'])
def sync_post(post_id):
    try:
        auth_header = request.headers.get('Authorization', '').split(' ')
        if len(auth_header) < 2 or len(auth_header[1]) == 0:
            return send_error('Sync request has invalid authorization token', 401)
        sb_token = auth_header[1]

        try:
            integrations = request.json['integrations']
            post = request.json['post']
        except BadRequest:
            return send_error('Request body must contain a valid JSON', 400)
        repos = read_integrations(integrations)

        try:
            api = sb_api.SbApi(sb_token)
        except HTTPError as err:
            logging.error("Error: %s." % err)
            return send_error('Error while initializing SB API - %s' % err, 400)

        for repository in repos:
            worker = PostSyncWorker(post, repository, api)
            worker.start()

        return 'OK'
    except:
        sync_fail('Error during synchronization!')
        raise


def sync_fail(msg):
    logging.error(msg)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
