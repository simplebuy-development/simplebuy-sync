class ProductConverter:
    def convert(self, product, repo=None):
        raise NotImplementedError('Please implement method to return ')


def optional(obj, value):
    return obj.value if hasattr(obj, value) else None
