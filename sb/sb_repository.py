from base_repository import TwoWaySyncRepository
from sb import sb_api
from sb.models.product import Product
from sb.models.chat import Chat


class SbRepository(TwoWaySyncRepository):
    def __init__(self, credentials, store_id):
        super().__init__(credentials)
        self.api = sb_api.SbApi(self.token)
        self.store_id = store_id
        self.store_avatar = ""
        store = self.get_store()
        self.store_name = store['name']

    def get_store(self):
        return self.api.store(self.store_id).get()

    @property
    def type(self):
        return 'sb'

    @property
    def two_way_sync(self) -> bool:
        return True

    @property
    def token(self):
        return self.credentials['token']

    def get_products_to_sync(self):
        return self.get_all_products()
        # store = self.api.store(self.store_id)
        # products = store.products().find_all({'filter[where][needSync][neq]': 'false'})
        # return [Product(**p) for p in products]

    def mark_as_synced(self, product, repo_type, external_id):
        sd = product.get_sync_data(repo_type)
        sd['id'] = external_id
        sd['revision'] = product.revision
        sd['lastSyncDate'] = product.update_date
        self.api.product(product.id).update_sync_data(product.sync_data, product.revision)
        return

    def get_all_products(self):
        store = self.api.store(self.store_id)
        products = store.products().find_all()
        return [Product(**p) for p in products]

    def save(self, product):
        # api = self.api.store(self.store_id)
        api = self.api
        if product.id:
            result = api.product(product.id).update(product)
        else:
            result = api.products().create(product)
        return result

    def get_all_chats(self):
        chats = self.api.chats().find_all({"filter": '{"where":{"storeId":{"like":"' + self.store_id + '"}}}'})
        return [Chat(**c) for c in chats]

    def create_chat(self, chat_data):
        res = self.api.chats().create(chat_data)
        return Chat(**res)

    def add_message(self, chat, message):
        chat = self.api.chat(chat.id)
        chat.add_comment(message)
