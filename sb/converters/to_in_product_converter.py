from instagram.models.instagram_post import InstagramPost
from product_converter import ProductConverter


class ToInProductConverter(ProductConverter):
    def convert(self, product, repo=None):
        hashtags = [("#" + ht["id"] if not ht["id"].startswith("#") else ht["id"]) for ht in product.hashtags]
        text = "%s за %sруб. Купить: http://simplebuy.org/product/%s/order\n%s %s" % (
            product.title, str(product.price), str(product.id), product.description, " ".join(hashtags)
        )
        photo = product.main_image
        if "no_photo" in photo:
            photo = "http://simplebuy.org/images/no_photo.png"

        return InstagramPost(photo, text)
