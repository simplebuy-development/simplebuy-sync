from datetime import date

from product_converter import ProductConverter
from sb.models.product import Product
from vk.models.market.market_item import MarketItem, MarketItemAvailability
from vk.vk_repository import VkRepository

DEFAULT_CATEGORY = {'id': 1, 'name': 'Женская одежда', 'section': {'id': 0, 'name': 'Гардероб'}}


class ToVkProductConverter(ProductConverter):
    def convert(self, product: Product, repo: VkRepository = None,
                store: dict = {}):

        tag_list = []
        for ht in product.hashtags:
            ht_label = ("#" if not ht['id'].startswith("#") else "") + ht['id']
            if ht_label not in product.description:
                tag_list.append(ht_label)

        if 'domain_name' in store.keys() and store['domain_name']:
            description='%s %s\n Купить: %s' % (product.description,
                                                ' '.join(tag_list),
                                                "http://%s/products/%s" % (store['domain_name'], product.id))
        elif 'store_id' in store.keys() and store['store_id']:
            description='%s %s\n Купить: %s' % (product.description,
                                                ' '.join(tag_list),
                                                "http://еком.рф/store/%s/products/%s" % (store['store_id'], product.id))
        else:
            description='%s %s' % (product.description, ' '.join(tag_list))

        res = MarketItem(
            id=product.get_sync_data('vk')['id'],
            owner_id=repo.owner_id,
            title=product.title,
            name=product.title,
            price={'amount': product.price},
            category=DEFAULT_CATEGORY,
            date=date.today(),
            description=description,
            thumb_photo=product.main_image,
            availability=MarketItemAvailability.available
        )
        return res
