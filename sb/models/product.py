import string
from typing import List

from sb.models.photo import Photo


class Product:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.store_id = kwargs['storeId']
        self.revision = kwargs.get('revision', 1)
        self.update_date = kwargs.get('updateDate')
        self.sync_data = kwargs.get('syncData', [])
        self.store_name = kwargs['storeName']
        self.title = kwargs['name']
        self.description = kwargs.get('description')
        self.price = kwargs['price']
        self.hashtags = kwargs.get('_hashtags', [])  # type: List[string]
        self.main_image = kwargs.get('mainImage')
        self.photos = [Photo(url=p) for p in kwargs.get('images')]  # type: List[Photo]

    def get_sync_data(self, repo_type):
        sync_data = next(filter(lambda sd: sd['type'] == repo_type, self.sync_data), None)
        return sync_data if sync_data else self.add_sync_data(repo_type)

    def is_expired(self, repo_type):
        sd = self.get_sync_data(repo_type)
        if not sd.get('id') or (sd.get('revision', 1) < self.revision):
            return True
        return False

    def add_sync_data(self, repo_type):
        sd = {
            'type': repo_type,
            'revision': 1,
            'id': None,
            'lastSyncDate': None
        }
        self.sync_data.append(sd)
        return sd
