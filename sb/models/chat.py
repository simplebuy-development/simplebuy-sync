class Chat:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.store_id = kwargs['storeId']
        self.chat_name = kwargs['chatName']
        self.message_count = len(kwargs['_messages'])
        self._messages = kwargs['_messages']
        self.object_id = kwargs['objectId']
        self.chat_type = kwargs['chatType']
        self.emotion_rate = kwargs['emotionRate']
        self.chat_image = kwargs['chatImage']
