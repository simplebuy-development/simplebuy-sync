from datetime import datetime

import requests

from sb.models.product import Product

URL_PREFIX = 'http://api.simplebuy.org'


def handle_response(response: requests.Response):
    if response.ok:
        return response.json()
    else:
        raise response.raise_for_status()


class ApiEndPoint(object):
    def __init__(self, parent):
        self.parent = parent

    def get_session(self):
        return self.parent.get_session()

    @property
    def url(self):
        raise NotImplementedError('Please implement this method: it should return partial url')


class ItemResource(ApiEndPoint):
    def __init__(self, parent, item_id):
        super().__init__(parent)
        self.item_id = item_id

    @staticmethod
    def partial_url():
        raise NotImplementedError('Please implement this method: it should return partial url')

    @property
    def url(self):
        return self.parent.url + self.partial_url() + '/' + self.item_id

    @staticmethod
    def serialize_item(item):
        raise NotImplementedError('Please implement this method: it should return json representation of item')

    def update(self, item):
        headers = {}
        if item.revision:
            headers['If-Match'] = str(item.revision)
        data = self.serialize_item(item)
        response = self.get_session().put(self.url, json=data, headers=headers)
        return handle_response(response)

    def get(self):
        response = self.get_session().get(self.url)
        return handle_response(response)


class ListResource(ApiEndPoint):
    def __init__(self, parent, item_class: type(ItemResource), partial_url=None):
        super().__init__(parent)
        self.item_class = item_class
        self.partial_url = partial_url if partial_url is not None else item_class.partial_url()

    @property
    def url(self):
        return self.parent.url + self.partial_url

    def item(self, item_id):
        return self.item_class(self, item_id)

    def find_all(self, params=None) -> requests.Session:
        response = self.get_session().get(self.url, params=params)
        result = handle_response(response)
        return result

    def create(self, item):
        data = self.item_class.serialize_item(item)
        data['creationDate'] = int(datetime.utcnow().timestamp())  # TODO: put into creation
        response = self.get_session().post(self.url, json=data)

        resp = handle_response(response)
        # Create chat for product
        if self.item_class == ProductsStoresSbApi:

            chat_data = {
                "chatName": "Товар - " + data['name'],
                "chatType": "SB_PRODUCT",
                "chatImage": data['mainImage'],
                "objectId": resp['id'],
                "storeId": data['storeId']
            }

            chat = ChatsSbApi(self.parent, None)
            chat.create_chat(chat_data)

        return resp


class SbApi:
    def __init__(self, token, api_version='v1'):
        self.token = token
        # self.user_id = user_id
        self.api_version = api_version
        self.url = URL_PREFIX + '/' + api_version

    def get_session(self):
        http = requests.Session()
        http.headers['Authorization'] = 'Bearer ' + self.token
        return http

    def products(self):
        return ListResource(self, ProductsStoresSbApi)

    def product(self, product_id):
        return ProductsStoresSbApi(self, product_id)

    def stores(self):
        return ListResource(self, StoreSbApi)

    def store(self, store_id):
        return StoreSbApi(self, store_id)

    def news(self, post_id):
        return NewsSbApi(self, post_id)

    def socials(self, social_id):
        return SocialsSbApi(self, social_id)

    def chat(self, chat_id):
        return ChatsSbApi(self, chat_id)

    def chats(self):
        return ListResource(self, ChatsSbApi)

    def domain(self, store_id):
        return StoreDomainApi(self, store_id)


class StoreDomainApi(ItemResource):
    def __init__(self, parent, store_id):
        super().__init__(parent, store_id)

    @staticmethod
    def partial_url():
        return '/domains'

    @property
    def url(self):
        return self.parent.url + self.partial_url() + '?filter={"where":{"store_id":{"like":"%s"}}}' % self.item_id

class StoreSbApi(ItemResource):
    def __init__(self, parent, store_id):
        super().__init__(parent, store_id)

    @staticmethod
    def partial_url():
        return '/stores'

    @staticmethod
    def serialize_item(item):
        pass

    def products(self):
        return ListResource(self, ProductsStoresSbApi)

    def product(self, product_id):
        return ProductsStoresSbApi(self, product_id)


class ProductsStoresSbApi(ItemResource):
    def __init__(self, parent, product_id):
        super().__init__(parent, product_id)

    @staticmethod
    def partial_url():
        return '/products'

    def update_sync_data(self, sync_data, etag=None):
        headers = {}
        if etag:
            headers['ETag'] = str(etag)
        self.get_session().put(self.url + '/syncData', json=sync_data, headers=headers)

    @staticmethod
    def serialize_item(item: Product):
        return {
            'id': item.id,
            'storeId': item.store_id,
            'revision': item.revision,
            'updateDate': item.update_date,
            'syncData': item.sync_data,
            'storeName': item.store_name,
            'name': item.title,
            'description': item.description,
            'price': item.price,
            '_hashtags': item.hashtags,
            'mainImage': item.main_image,
            'images': [photo.url for photo in item.photos]
        }


class ChatsSbApi(ItemResource):

    @staticmethod
    def partial_url():
        return '/chats'

    def update_sync_data(self, sync_data):
        pass

    @staticmethod
    def serialize_item(item):
        return item

    def add_comment(self, comment):
        self.get_session().post(self.url + '/messages', json=comment)

    def create_chat(self, chat):
        self.get_session().post(self.parent.url + self.partial_url(), json=chat)


class NewsSbApi(ItemResource):
    def __init__(self, parent, post_id):
        super().__init__(parent, post_id)

    @staticmethod
    def partial_url():
        return '/news'

    @staticmethod
    def serialize_item(item):
        pass

    def update_sync_data(self, sync_data):
        self.get_session().put(self.url + '/syncData', json=sync_data)


class SocialsSbApi(ItemResource):
    def __init__(self, parent, post_id):
        super().__init__(parent, post_id)

    @staticmethod
    def partial_url():
        return '/socials'

    @staticmethod
    def serialize_item(item):
        pass

    def update_sync_data(self, sync_data):
        self.get_session().put(self.url, json=sync_data)
