FROM python:3
MAINTAINER Aleksei Lebedev <allebdev@gmail.com>
# Add requirements (and create src dir by copying)
ADD requirements.txt /src/requirements.txt
# Switch to src folder
WORKDIR src
# Install requirements
RUN pip install -r requirements.txt
# Add application source code
ADD ./ .
# Expose default Flask port
EXPOSE 5000
#
CMD ["python", "/src/main.py"]